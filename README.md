# Система идентификации человека по лицу
Репозиторий содержит Docker окружение для сборки и тестирования проекта для идентификации человека по лицу.

## Требования для запуска
0. git
1. Docker 20.10 или выше.
2. Настроенный [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) для запуска на GPU (вычисления на порядки быстрее), который поддерживает CUDA=11.1.

## Подготовка данных 
В директории `GT` необходимо сохранить эталонные портетные фотографии людей. В текстовой БД `lightness_db.json` необходимо указать данные человека, заполнив следующие поля:

 * "id": str -- идентификатор
 * "name": str -- ФИО
 * "position": str -- должность
 * "gt_path": str -- путь до эталонной фотографии (`GT/image.jpg`)


## Запуск проекта

Клонировать данный репозиторий:
```
git clone https://bitbucket.org/purity666/facerecognition.git
```

### Как Docker контейнер

Выполнить команду
```
docker-compose up --build face_recognition_[c,g]pu
```
В `docker-compose.yml` можно конфигурировать внешний порт веб-сервиса через переменную `PORT`. По умолчанию он 5000.

### Как conda окружение

Создать окружение, используя `environment.yml`, активировать его:
```
conda env create --name NAME --file environment.yml
conda activate NAME
```

Если не нужно создавать отдельное окружение, то можно установить зависимости с помощью pip:
```
pip install -r requirements.txt
```

Необходимо скачать архивированные предобученные веса моделей и распаковать их:
```
wget https://github.com/davisking/dlib-models/raw/master/mmod_human_face_detector.dat.bz2
wget https://github.com/davisking/dlib-models/raw/master/shape_predictor_68_face_landmarks.dat.bz2
wget https://github.com/davisking/dlib-models/raw/master/dlib_face_recognition_resnet_model_v1.dat.bz2
bzip2 -d mmod_human_face_detector.dat.bz2
bzip2 -d shape_predictor_68_face_landmarks.dat.bz2
bzip2 -d dlib_face_recognition_resnet_model_v1.dat.bz2
```

Запустить сервис:
```
python app.py
```

По адресу [localhost:5000](localhost:5000) будет доступна форма заполнения данных. Порт сервиса можно изменить в скрипте `app.py`. По умолчанию он 5000.

## Тестирование проекта

Необходимо загрузить изображение, содержащее лицо, и указать идентификационный номер человека. Далее нажать кнопку `Сканировать`.

Загруженные изображения будут сохранены в директории `uploads`.

В результате будет сгенерирован json следующего содержания:
```
{
    "confidence": float, -- чем ближе к 1, тем больше уверенность того, что это тот же человек
    "error": str, -- сообщение об ошибке
    "face_num": int, -- количество обнаруженных лиц на изображении
    "id_employee": str, -- идентификатор человека
    "name_employee": str, -- ФИО
    "position": str, -- должность
    "threshold": float -- порог классификации
}
```

В зависимости от типа ошибок содеримое ответа может варьироваться.

Возможные варианты ошибок:

 * No file.
 * Unsupported image extension (not in ['png', 'jpg', 'jpeg'])
 * идентификационный номер отсутствует в БД.
 * several employees found with id="".
 * no GT found with id="".
 * нет лиц в кадре.
 * больше одного лица в кадре.
 * другой человек в кадре.

Для распознавания человека используется библиотека [dlib](https://github.com/davisking/dlib) с предобученной моделью [dlib_face_recognition_resnet_model_v1](https://github.com/davisking/dlib-models/raw/master/dlib_face_recognition_resnet_model_v1.dat.bz2). При сравнении двух лиц используется L2-норма, при значении < 0.6 можно считать, что это один и тот же человек. Данный порог `COMPARISON_THRESHOLD` можно изменить в скрипте `app.py`.
