import logging
from pathlib import Path

from flask import Flask, render_template, request, Response
from tinydb import TinyDB, Query

from face_utils import FaceHelper

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

db = TinyDB('lightness_db.json')

# see FaceHelper constructor to pass custom model paths
face_helper = FaceHelper()
face_helper.init_dlib()

upload_folder = Path('uploads')
try:
    upload_folder.mkdir()
except FileExistsError:
    logging.warning(f'{upload_folder} is exists.')

COMPARISON_THRESHOLD = 0.6
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']
FORM_HTML = 'form.html'


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_data():
    if request.method == 'GET':
        return render_template(FORM_HTML)

    result = {}
    result['error'] = ''

    file = request.files['file']
    if file.filename == '':
        result['error'] += 'No file.'
        return result

    if file and allowed_file(file.filename):
        input_path = upload_folder / file.filename
        file.save(input_path)
    else:
        result['error'] += \
            f'Unsupported image extension (not in {ALLOWED_EXTENSIONS}).'
        return result

    # let id be uniqueness
    id = request.form['id']

    employee_property = Query()
    employee_data = db.search(employee_property.id == id)
    if not len(employee_data):
        result['error'] += 'идентификационный номер отсутствует в БД.'
        return result
    elif len(employee_data) > 1:
        result['error'] += f'several employees found with id={id}.'
        return result

    employee_data = employee_data[0]

    gt_path = Path(employee_data['gt_path'])
    if not gt_path.is_file():
        result['error'] += f'no GT found with id={id}.'
        return result

    l2_norm, face_info = face_helper.compare_faces(str(input_path),
                                                   str(gt_path))
    if l2_norm is None:
        result['error'] += 'нет лиц в кадре.'
        return result

    if face_info > 1:
        result['error'] += 'больше одного лица в кадре.'

    if l2_norm > COMPARISON_THRESHOLD:
        result['error'] += 'другой человек в кадре.'

    result['confidence'] = 1 - l2_norm #???
    result['face_num'] = face_info
    result['name_employee'] = employee_data['name']
    result['position'] = employee_data['position']
    result['id_employee'] = employee_data['id']
    result['threshold'] = COMPARISON_THRESHOLD

    return result


if __name__ == '__main__':
    app.run(host='0.0.0.0')
    face_helper.dlib_free()
    db.close()
