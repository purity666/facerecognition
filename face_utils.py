import logging
try:
    import dlib
except ImportError:
    logging.error('Please install dlib before testing face recognition.' 'Reference:　https://github.com/davisking/dlib')

import numpy as np


class FaceHelper:
    def __init__(self, model_detector_path='mmod_human_face_detector.dat',
                 model_sp_path='shape_predictor_68_face_landmarks.dat',
                 model_recogniser_path='dlib_face_recognition_resnet_model_v1.dat'):
        self.model_detector_path = model_detector_path
        self.model_sp_path = model_sp_path
        self.model_recogniser_path = model_recogniser_path
        self.img = None
        self.det_face = None
        self.face_descriptor = None

    def init_dlib(self):
        self.face_detector = dlib.cnn_face_detection_model_v1(
                                            self.model_detector_path)
        self.shape_predictor = dlib.shape_predictor(self.model_sp_path)
        self.face_recogniser = dlib.face_recognition_model_v1(
                                            self.model_recogniser_path)

    def dlib_free(self):
        del self.face_detector
        del self.shape_predictor
        del self.face_recogniser

    def _read_image(self, img_path):
        img = dlib.load_rgb_image(img_path)
        self.img = img
        return img

    def detect_face(self, img_path, upsample_num_times=1):
        '''
            img_path (str): Image path.
            upsample_num_times (int): Upsamples the image before running the
                face detector
        '''
        input_img = self._read_image(img_path)
        det_faces = self.face_detector(input_img, upsample_num_times)

        if not len(det_faces):
            logging.warning('No faces found. \
                Try to increase upsample_num_times')
        else:
            if len(det_faces) > 1:
                logging.warning('Several faces found')
                # keep the largest
                face_areas = []
                for i in range(len(det_faces)):
                    face_area = (det_faces[i].rect.right()-det_faces[i].rect.left())*(
                                 det_faces[i].rect.bottom()-det_faces[i].rect.top())
                    face_areas.append(face_area)
                largest_idx = face_areas.index(max(face_areas))
                self.det_face = det_faces[largest_idx]
            else:
                self.det_face = det_faces[0]
        return self.det_face, len(det_faces)

    def _get_face_descriptor(self):
        full_object_detection = self.shape_predictor(self.img, self.det_face.rect)
        self.face_descriptor = self.face_recogniser.compute_face_descriptor(self.img, full_object_detection)
        return np.array(self.face_descriptor)

    def compare_faces(self, source_path, target_path):
        source_detect, face_info = self.detect_face(source_path)
        if source_detect is None:
            return None, "input image contain no face!"
        source_descriptor = self._get_face_descriptor()

        target_detect = self.detect_face(target_path)
        if target_detect is None:
            return None, "GT image contain no face!"
        target_descriptor = self._get_face_descriptor()

        # l2_norm: lower is better. dlib model provide true if less 0.6
        l2_norm = np.linalg.norm(target_descriptor - source_descriptor)
        return l2_norm, face_info


if __name__ == '__main__':
    import argparse

    args = argparse.ArgumentParser()
    args.add_argument('--source', type=str, required=True,
                      help='Source photo path.')
    args.add_argument('--target', type=str, required=True,
                      help='Target photo path.')
    args.add_argument('--tolerance', type=float, default=1e-1, required=False,
                      help='Tolerance for confidense.')
    args = args.parse_args()

    face_helper = FaceHelper()
    face_helper.init_dlib()

    result = face_helper.compare_faces(args.source, args.target)
    # less 0.6 -- same person
    print(result)

    face_helper.dlib_free()
